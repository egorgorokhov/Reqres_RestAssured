package reqres;

import api.models.*;
import api.steps.ReqresSteps;
import com.fasterxml.jackson.databind.introspect.TypeResolutionContext;
import io.restassured.response.Response;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class TestReqres {

    //Позитивный, POST /register
    @Test
    public void successRegister() {
        RegisterUserPayload payload = new RegisterUserPayload("tobias.funke@reqres.in", "daffodil");

        RegisterSuccessResponse response = ReqresSteps.postRegisterSuccess(payload);

        ReqresSteps.checkRegisterSuccess(response);
    }

    //Негативный, POST /register
    @Test
    public void unsuccessfulRegister() {
        RegisterUserPayload payload = new RegisterUserPayload("gladius.fif@qaboss.com", "");

        RegisterFailureResponse response = ReqresSteps.postRegisterFailure(payload);

        ReqresSteps.checkRegisterFailure(response);
    }

    //Позитивный, PUT, /users/{id}
    @Test
    public void successPutUpdate() {
        UpdateUserPayload payload = new UpdateUserPayload("alejandro", "colonel");

        UpdateSuccessResponse response = ReqresSteps.putUpdateSuccess("5", payload);

        ReqresSteps.checkUpdateSuccess(response, payload);
    }

    //Негативный, PUT, /users/{id}
    @Test
    public void failurePutUpdate() {
        UpdateUserPayload payload = new UpdateUserPayload();

        Response response = ReqresSteps.putUpdateFailure("10", payload);

    }

    //Позитивный, DELETE
    @Test
    public void positiveDeleteUser() {
        Response response = ReqresSteps.deleteResponse("4");
        Assertions.assertTrue(response.contentType().isEmpty());
    }

    //Негативный, DELETE
    @Test
    public void negativeDeleteUser() {
        Response response = ReqresSteps.deleteResponse("gibberish");
        Assertions.assertEquals(204, response.statusCode());
    }

    //Позитивный, GET /user/{id}, проверяем что email кончается на @reqres.in
    @Test
    public void successGetSingleUser() {
        SingleUserResponse response = ReqresSteps.getSingleUserSuccess(4);

        ReqresSteps.checkEmail(response);
    }

    //Негативный, GET /user/{id}, проверка что тело ответа пусто
    @Test
    public void failureGetSingleUser() {
        SingleUserResponse response = ReqresSteps.getSingleUserFailure(777);

        Assertions.assertEquals(null, response);

    }
}
