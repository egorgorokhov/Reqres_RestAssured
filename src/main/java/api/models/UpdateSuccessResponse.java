package api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.junit.Ignore;

@AllArgsConstructor
@Getter
public class UpdateSuccessResponse {
    private String name;

    private String job;

    @JsonIgnoreProperties
    private String updatedAt;
}
