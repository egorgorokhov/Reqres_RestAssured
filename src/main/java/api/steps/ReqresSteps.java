package api.steps;

import api.models.*;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;

import static io.restassured.RestAssured.given;

public class ReqresSteps {

    public static RegisterSuccessResponse postRegisterSuccess(RegisterUserPayload payload) {
        return given()
                .spec(SpecHelper.requestSpec())
                .when()
                .body(payload)
                .post("register")
                .then()
                .spec(SpecHelper.responseSpec(200))
                .extract()
                .as(RegisterSuccessResponse.class);
    }

    public static RegisterFailureResponse postRegisterFailure(RegisterUserPayload payload) {
        return given()
                .spec(SpecHelper.requestSpec())
                .when()
                .body(payload)
                .post("register")
                .then()
                .spec(SpecHelper.responseSpec(400))
                .extract()
                .as(RegisterFailureResponse.class);
    }

    public static void checkRegisterSuccess(RegisterSuccessResponse response) {
        Assertions.assertNotNull(response.getId());
        Assertions.assertNotNull(response.getToken());
    }

    public static void checkRegisterFailure(RegisterFailureResponse response) {
        Assertions.assertEquals("Missing password", response.getError());
    }

    public static UpdateSuccessResponse putUpdateSuccess(String id, UpdateUserPayload payload) {
        return given()
                .spec(SpecHelper.requestSpec())
                .when()
                .body(payload)
                .put(String.format("users/%s", id))
                .then()
                .spec(SpecHelper.responseSpec(200))
                .extract()
                .as(UpdateSuccessResponse.class);
    }

    public static Response putUpdateFailure(String id, UpdateUserPayload payload) {
        return given()
                .spec(SpecHelper.requestSpec())
                .when()
                .body(payload)
                .put(String.format("users/%s", id))
                .then()
                .spec(SpecHelper.responseSpec(400))
                .extract()
                .response();
    }

    public static void checkUpdateSuccess(UpdateSuccessResponse response, UpdateUserPayload payload) {
        Assertions.assertEquals(payload.getName(), response.getName());
        Assertions.assertEquals(payload.getJob(), response.getJob());
    }

    public static Response deleteResponse(String id) {
        return given()
                .spec(SpecHelper.requestSpec())
                .when()
                .delete(String.format("users/%s", id))
                .then()
                .spec(SpecHelper.responseSpec(204))
                .extract()
                .response();
    }

    public static SingleUserResponse getSingleUserSuccess(Integer id) {
        return given()
                .spec(SpecHelper.requestSpec())
                .when()
                .get(String.format("users/%s", id.toString()))
                .then()
                .spec(SpecHelper.responseSpec(200))
                .extract()
                .body().jsonPath().getObject("data",SingleUserResponse.class);
    }

    public static SingleUserResponse getSingleUserFailure(Integer id) {
        return given()
                .spec(SpecHelper.requestSpec())
                .when()
                .get(String.format("users/%s", id.toString()))
                .then()
                .spec(SpecHelper.responseSpec(404))
                .extract()
                .body().jsonPath().getObject("data",SingleUserResponse.class);

    }

    public static void checkEmail(SingleUserResponse response) {
        Assert.assertTrue(response.getEmail().endsWith("@reqres.in"));
    }

}
